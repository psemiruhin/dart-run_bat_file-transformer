Transformer to run .bat file

## Usage

Simply add the following lines to your `pubspec.yaml`:

    :::yaml
    dependencies:
      run_bat_file: any
    transformers:
      - run_bat_file

## Configuration

You can specify executable file:

    :::yaml
    transformers:
      - run_bat_file:
          executable: /path/to/file.bat
