library run_file.transformer;

import 'dart:async';
import 'package:barback/barback.dart';
import 'package:path/path.dart';
import 'dart:io';

/// Transformer used by `pub build` and to run .bat files.
class RunFileTransformer extends Transformer {
  final BarbackSettings settings;
  final TransformerOptions options;

  RunFileTransformer(BarbackSettings settings) :
    settings = settings,
    options = new TransformerOptions.parse(settings.configuration);

  RunFileTransformer.asPlugin(BarbackSettings settings) :
    this(settings);

  Future<bool> isPrimary(AssetId input) {
    var extension = posix.extension(input.path);
    bool primary = extension == '.bat';
    if (options.executable != null && primary) {
      if (posix.basename(input.path) == posix.basename(options.executable)) {
        primary = true;
      }
    } else {
      primary = false;
    }
    
    return new Future.value(primary);
  }

  Future apply(Transform transform) {
    String path = options.executable;

    transform.consumePrimary();

//    print("try to run " + path);
    return Process.run(path, ['']).then((ProcessResult result) {
      print("run " + path);
      return;
    }).catchError((ProcessException e) {
      print(e.toString());
    });
  }
}

class TransformerOptions {
  final String executable;

  TransformerOptions({String this.executable});

  factory TransformerOptions.parse(Map configuration) {
    config(key, defaultValue) {
      var value = configuration[key];
      return value != null ? value : defaultValue;
    }

    return new TransformerOptions(
        executable: config("executable", null));
  }
}